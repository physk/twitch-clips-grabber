const axios = require('axios');
const mri = require('mri');
const argv = process.argv.slice(2);
var mriv = mri(argv, { clientID:false, channelName:false });
let arrayURL = [];

//console.log(mriv);
if(mriv.clientID == false) {
    console.log("# You have not entered a client id");
    console.log('# Use --clientID="your_client_id"');
    process.exit(1);
}

if(mriv.channelName == false) {
    console.log("# You have not entered a Channel Name");
    console.log('# --channelName="shroud"');
    process.exit(1);
}

axios({
    url: 'https://api.twitch.tv/helix/users?login='+mriv.channelName,
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Client-ID': mriv.clientID
    }
})
.then((responce) => {
    if(responce.data.length == 0) {
        console.log("# You have either entered the wrong channel name or there are no past broadcasts");
        process.exit(1);
    }
    var id = responce.data.data[0].id;
    axios({
        url: 'https://api.twitch.tv/helix/clips?broadcaster_id='+id+'&first=100',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Client-ID': mriv.clientID
        }
    })
    .then((responce) => {
        if(responce.data.length == 0) {
            console.log("# You have either entered the wrong channel name or there are no clips");
            process.exit(1);
        }
        responce.data.data.forEach((data) => {
            arrayURL.push(data);
        });
        if(responce.data.pagination.cursor !== undefined) {
            var cursor = responce.data.pagination.cursor;
            getNextPage(id, cursor);
        }
        else {
            printArray();
        }}
    );
})
function getNextPage(id, cursor) {
    axios({
        url: 'https://api.twitch.tv/helix/clips?broadcaster_id='+id+'&first=100&after='+cursor,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Client-ID': mriv.clientID
        }
    })
    .then((responce) => {
        if(responce.data.length == 0) {
            console.log("# You have either entered the wrong channel name or there are no more clips");
            process.exit(1);
        }
        responce.data.data.forEach((data) => {
            arrayURL.push(data);
        });
        if(responce.data.pagination.cursor !== undefined) {
            var cursor = responce.data.pagination.cursor;
            getNextPage(id, cursor);
        }
        else {
            printArray();
        }}
    );
}
function printArray() {
    arrayURL.forEach((element) => {
        console.log(element.url)
    })
}